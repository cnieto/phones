package es.cnieto.phones.repository.memory;

import es.cnieto.phones.domain.Phone;
import es.cnieto.phones.domain.PhonesRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PhonesMemoryRepository implements PhonesRepository {

    private final Map<String, Phone> phones;

    public PhonesMemoryRepository(Phone... phones) {
        this.phones = Stream.of(phones).collect(Collectors.toMap(Phone::getId, Function.identity()));
    }

    @Override
    public List<Phone> findAll() {
        return new ArrayList<>(phones.values());
    }

    @Override
    public Optional<Phone> findBy(String id) {
        return Optional.ofNullable(phones.get(id));
    }


}
