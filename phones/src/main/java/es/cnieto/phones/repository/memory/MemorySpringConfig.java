package es.cnieto.phones.repository.memory;

import es.cnieto.phones.domain.Phone;
import es.cnieto.phones.domain.PhonesRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;
import java.net.URI;

@Configuration
public class MemorySpringConfig {
    @Bean
    public PhonesRepository phonesRepository() {
        return new PhonesMemoryRepository(
                new Phone("1",
                        URI.create("http://image.one/"),
                        "phone one",
                        "this is the phone one",
                        BigDecimal.valueOf(233.20)),
                new Phone("2",
                        URI.create("http://image.two/"),
                        "phone two",
                        "this is the phone two",
                        BigDecimal.valueOf(120.50)));
    }
}
