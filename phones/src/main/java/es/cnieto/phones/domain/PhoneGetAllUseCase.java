package es.cnieto.phones.domain;

import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class PhoneGetAllUseCase {

    private final PhonesRepository phonesRepository;

    public List<Phone> getAll() {
        return phonesRepository.findAll();
    }
}
