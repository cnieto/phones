package es.cnieto.phones.domain;

import java.util.List;
import java.util.Optional;

public interface PhonesRepository {
    List<Phone> findAll();

    Optional<Phone> findBy(String id);
}
