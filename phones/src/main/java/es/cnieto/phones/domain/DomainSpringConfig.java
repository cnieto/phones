package es.cnieto.phones.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DomainSpringConfig {
    private final PhonesRepository phonesRepository;

    public DomainSpringConfig(PhonesRepository phonesRepository) {
        this.phonesRepository = phonesRepository;
    }

    @Bean
    public PhoneGetAllUseCase phoneGetAllUseCase() {
        return new PhoneGetAllUseCase(phonesRepository);
    }

    @Bean
    public PhoneGetUseCase phoneGetUseCase() {
        return new PhoneGetUseCase(phonesRepository);
    }
}
