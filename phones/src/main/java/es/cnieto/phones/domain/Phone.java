package es.cnieto.phones.domain;

import lombok.Data;

import java.math.BigDecimal;
import java.net.URI;

@Data
public class Phone {
    private final String id;
    private final URI image;
    private final String name;
    private final String description;
    private final BigDecimal price;
}
