package es.cnieto.phones.domain;

import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
public class PhoneGetUseCase {

    private final PhonesRepository phonesRepository;

    public Optional<Phone> get(String id) {
        return phonesRepository.findBy(id);
    }
}
