package es.cnieto.phones.rest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RestSpringConfig {
    @Bean
    public PhoneApiConverter phoneApiConverter() {
        return new PhoneApiConverter();
    }
}
