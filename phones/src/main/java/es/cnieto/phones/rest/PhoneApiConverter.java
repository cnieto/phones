package es.cnieto.phones.rest;

import es.cnieto.phones.domain.Phone;

public class PhoneApiConverter {
    public PhoneApi from(Phone phone) {
        PhoneApi phoneApi = new PhoneApi();
        phoneApi.setId(phone.getId());
        phoneApi.setName(phone.getName());
        phoneApi.setDescription(phone.getDescription());
        phoneApi.setImage(phone.getImage());
        phoneApi.setPrice(phone.getPrice());

        return phoneApi;
    }
}
