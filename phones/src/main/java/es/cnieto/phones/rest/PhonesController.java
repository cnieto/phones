package es.cnieto.phones.rest;

import es.cnieto.phones.domain.PhoneGetAllUseCase;
import es.cnieto.phones.domain.PhoneGetUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/phones")
@RequiredArgsConstructor
public class PhonesController {
    private final PhoneGetAllUseCase phoneGetAllUseCase;
    private final PhoneGetUseCase phoneGetUseCase;
    private final PhoneApiConverter phoneApiConverter;

    @RequestMapping(method = RequestMethod.GET,
            value = "",
            produces = APPLICATION_JSON_VALUE)
    public List<PhoneApi> getPhones() {
        return phoneGetAllUseCase.getAll().stream()
                .map(phoneApiConverter::from)
                .collect(toList());
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "/{phoneId}",
            produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PhoneApi> getPhone(@PathVariable String phoneId) {
        Optional<PhoneApi> phoneOptional = phoneGetUseCase.get(phoneId).map(phoneApiConverter::from);

        if (phoneOptional.isPresent()) {
            return ResponseEntity.ok(phoneOptional.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
