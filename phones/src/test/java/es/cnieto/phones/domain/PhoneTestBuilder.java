package es.cnieto.phones.domain;

import java.math.BigDecimal;
import java.net.URI;

public final class PhoneTestBuilder {
    private String id = "id";
    private URI image = URI.create("http://image/");
    private String name = "name";
    private String description = "description";
    private BigDecimal price = BigDecimal.valueOf(23.33);

    private PhoneTestBuilder() {
    }

    public static PhoneTestBuilder aPhone() {
        return new PhoneTestBuilder();
    }

    public PhoneTestBuilder withId(String id) {
        this.id = id;
        return this;
    }

    public PhoneTestBuilder withImage(URI image) {
        this.image = image;
        return this;
    }

    public PhoneTestBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public PhoneTestBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public PhoneTestBuilder withPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public Phone build() {
        return new Phone(id, image, name, description, price);
    }
}
