package es.cnieto.phones.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static es.cnieto.phones.domain.PhoneTestBuilder.aPhone;
import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PhoneGetAllUseCaseTest {
    private static final String FIRST_PHONE_ID = "phoneOneId";
    private static final String SECOND_PHONE_ID = "phoneTwoId";

    private PhoneGetAllUseCase phoneGetAllUseCase;
    @Mock
    private PhonesRepository phonesRepository;

    @Before
    public void setup() {
        this.phoneGetAllUseCase = new DomainSpringConfig(phonesRepository).phoneGetAllUseCase();
    }

    @Test
    public void getAllUseCase() {
        when(phonesRepository.findAll()).thenReturn(phoneList());

        List<Phone> phones = phoneGetAllUseCase.getAll();

        assertThat(phones, equalTo(phoneList()));
    }

    private List<Phone> phoneList() {
        return asList(
                aPhone().withId(FIRST_PHONE_ID).build(),
                aPhone().withId(SECOND_PHONE_ID).build()
        );
    }
}