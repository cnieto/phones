package es.cnieto.phones.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static es.cnieto.phones.domain.PhoneTestBuilder.aPhone;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PhoneGetUseCaseTest {
    private static final String PHONE_ID = "phoneId";

    private PhoneGetUseCase phoneGetUseCase;
    @Mock
    private PhonesRepository phonesRepository;

    @Before
    public void setup() {
        this.phoneGetUseCase = new DomainSpringConfig(phonesRepository).phoneGetUseCase();
    }

    @Test
    public void getUseCase() {
        when(phonesRepository.findBy(PHONE_ID)).thenReturn(Optional.of(aPhone().build()));

        Optional<Phone> phoneOptional = phoneGetUseCase.get(PHONE_ID);

        assertThat(phoneOptional, equalTo(Optional.of(aPhone().build())));
    }

    @Test
    public void getUseCaseWithNonExistentId() {
        when(phonesRepository.findBy(PHONE_ID)).thenReturn(Optional.empty());

        Optional<Phone> phoneOptional = phoneGetUseCase.get(PHONE_ID);

        assertFalse(phoneOptional.isPresent());
    }
}