package es.cnieto.phones.rest;

import es.cnieto.phones.domain.Phone;
import org.junit.Test;

import java.math.BigDecimal;
import java.net.URI;

import static es.cnieto.phones.domain.PhoneTestBuilder.aPhone;
import static es.cnieto.phones.rest.PhoneApiTestBuilder.aPhoneApi;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class PhoneApiConverterTest {
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final URI IMAGE = URI.create("http://image");
    private static final String DESCRIPTION = "description";
    private static final BigDecimal PRICE = BigDecimal.valueOf(23.22);
    private PhoneApiConverter phoneApiConverter = new RestSpringConfig().phoneApiConverter();

    @Test
    public void convertSuccessfully() {
        Phone phone = aPhone().withId(ID)
                .withName(NAME)
                .withImage(IMAGE)
                .withDescription(DESCRIPTION)
                .withPrice(PRICE)
                .build();

        PhoneApi phoneApi = phoneApiConverter.from(phone);

        assertThat(phoneApi,
                equalTo(aPhoneApi()
                        .withId(ID)
                        .withName(NAME)
                        .withImage(IMAGE)
                        .withDescription(DESCRIPTION)
                        .withPrice(PRICE)
                        .build()));
    }
}