package es.cnieto.phones.rest;

import java.math.BigDecimal;
import java.net.URI;

public final class PhoneApiTestBuilder {
    private String id;
    private URI image;
    private String name;
    private String description;
    private BigDecimal price;

    private PhoneApiTestBuilder() {
    }

    public static PhoneApiTestBuilder aPhoneApi() {
        return new PhoneApiTestBuilder();
    }

    public PhoneApiTestBuilder withId(String id) {
        this.id = id;
        return this;
    }

    public PhoneApiTestBuilder withImage(URI image) {
        this.image = image;
        return this;
    }

    public PhoneApiTestBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public PhoneApiTestBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public PhoneApiTestBuilder withPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public PhoneApi build() {
        PhoneApi phoneApi = new PhoneApi();
        phoneApi.setId(id);
        phoneApi.setImage(image);
        phoneApi.setName(name);
        phoneApi.setDescription(description);
        phoneApi.setPrice(price);
        return phoneApi;
    }
}
