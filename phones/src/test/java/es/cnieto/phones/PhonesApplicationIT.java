package es.cnieto.phones;

import es.cnieto.phones.domain.PhonesRepository;
import es.cnieto.phones.repository.memory.PhonesMemoryRepository;
import es.cnieto.phones.rest.PhoneApi;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.net.URI;

import static es.cnieto.phones.domain.PhoneTestBuilder.aPhone;
import static es.cnieto.phones.rest.PhoneApiTestBuilder.aPhoneApi;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.core.IsEqual.equalTo;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PhonesApplicationIT {
    private static final String FIRST_PHONE_ID = "phoneOneId";
    private static final URI FIRST_PHONE_IMAGE = URI.create("http://phone.one/image");
    private static final String FIRST_PHONE_NAME = "phoneOneName";
    private static final String FIRST_PHONE_DESCRIPTION = "phoneOneDescription";
    private static final BigDecimal FIRST_PHONE_PRICE = BigDecimal.valueOf(20.32);

    private static final String SECOND_PHONE_ID = "phoneTwoId";
    private static final URI SECOND_PHONE_IMAGE = URI.create("http://phone.two/image");
    private static final String SECOND_PHONE_NAME = "phoneTwoName";
    private static final String SECOND_PHONE_DESCRIPTION = "phoneTwoDescription";
    private static final BigDecimal SECOND_PHONE_PRICE = BigDecimal.valueOf(110.58);

    private static final String NON_EXISTENT_PHONE_ID = "none";

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void contextLoads() {
    }

    @Test
    public void getAllPhones() {
        PhoneApi[] phones = restTemplate.getForObject("/phones", PhoneApi[].class);

        assertThat(asList(phones), containsInAnyOrder(firstPhoneApi(), secondPhoneApi()));
    }

    @Test
    public void getOnePhone() {
        PhoneApi phoneApi = restTemplate.getForObject("/phones/" + FIRST_PHONE_ID, PhoneApi.class);

        assertThat(phoneApi, equalTo(firstPhoneApi()));
    }

    @Test
    public void getOneNonExistentPhone() {
        ResponseEntity<PhoneApi> phoneApiResponseEntity = restTemplate.getForEntity("/phones/" + NON_EXISTENT_PHONE_ID, PhoneApi.class);

        assertThat(phoneApiResponseEntity.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
    }

    private PhoneApi firstPhoneApi() {
        return aPhoneApi().withId(FIRST_PHONE_ID)
                .withImage(FIRST_PHONE_IMAGE)
                .withName(FIRST_PHONE_NAME)
                .withDescription(FIRST_PHONE_DESCRIPTION)
                .withPrice(FIRST_PHONE_PRICE)
                .build();
    }

    private PhoneApi secondPhoneApi() {
        return aPhoneApi().withId(SECOND_PHONE_ID)
                .withImage(SECOND_PHONE_IMAGE)
                .withName(SECOND_PHONE_NAME)
                .withDescription(SECOND_PHONE_DESCRIPTION)
                .withPrice(SECOND_PHONE_PRICE)
                .build();
    }

    @TestConfiguration
    static class RepositoryConfiguration {
        @Bean
        @Primary
        public PhonesRepository phonesRepository() {
            return new PhonesMemoryRepository(
                    aPhone().withId(FIRST_PHONE_ID)
                            .withImage(FIRST_PHONE_IMAGE)
                            .withName(FIRST_PHONE_NAME)
                            .withDescription(FIRST_PHONE_DESCRIPTION)
                            .withPrice(FIRST_PHONE_PRICE)
                            .build(),
                    aPhone().withId(SECOND_PHONE_ID)
                            .withImage(SECOND_PHONE_IMAGE)
                            .withName(SECOND_PHONE_NAME)
                            .withDescription(SECOND_PHONE_DESCRIPTION)
                            .withPrice(SECOND_PHONE_PRICE)
                            .build()
            );
        }
    }
}