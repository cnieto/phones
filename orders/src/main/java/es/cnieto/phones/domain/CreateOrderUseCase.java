package es.cnieto.phones.domain;

import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@RequiredArgsConstructor
public class CreateOrderUseCase {
    private final PhonesRepository phoneRepository;
    private final OrdersRepository ordersRepository;
    private final LogRepository logRepository;

    public OrderResponse create(OrderRequest orderRequest) {
        OrderResponse orderResponse = new OrderResponse();
        List<Phone> phones = getPhones(orderRequest, orderResponse);

        if (!orderResponse.hasErrors()) {
            Order order = ordersRepository.persist(orderRequest.getName(),
                    orderRequest.getSurname(),
                    orderRequest.getEmail(),
                    phones);
            logRepository.info("Order for [name:{}][surname:{}][email:{}][phones:{}] created with a total price of {}",
                    order.getName(),
                    order.getSurname(),
                    order.getEmail(),
                    getPhonesForLog(order),
                    order.getTotalPrice());
        }

        return orderResponse;
    }

    private String getPhonesForLog(Order order) {
        return String.join(", ", order.getPhones().stream().map(Phone::getId).collect(toList()));
    }

    private List<Phone> getPhones(OrderRequest orderRequest, OrderResponse orderResponse) {

        List<Phone> phones = new ArrayList<>();
        orderRequest.getPhoneIds().forEach(phoneId -> addOnListOrAddError(phoneId, phones, orderResponse));
        return phones;
    }

    private void addOnListOrAddError(String phoneId, List<Phone> phones, OrderResponse orderResponse) {
        Optional<Phone> phoneOptional = phoneRepository.findBy(phoneId);
        phoneOptional.ifPresent(phones::add);
        if (!phoneOptional.isPresent()) {
            orderResponse.addPhoneNotFoundError(phoneId);
        }
    }
}
