package es.cnieto.phones.domain;

import lombok.Data;

import java.util.List;

@Data
public class OrderRequest {
    private final String name;
    private final String surname;
    private final String email;
    private final List<String> phoneIds;
}
