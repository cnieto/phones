package es.cnieto.phones.domain;

import java.util.Optional;

public interface PhonesRepository {
    Optional<Phone> findBy(String phoneId);
}
