package es.cnieto.phones.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DomainSpringConfig {
    private final OrdersRepository ordersRepository;
    private final PhonesRepository phonesRepository;
    private final LogRepository logRepository;

    public DomainSpringConfig(OrdersRepository ordersRepository, PhonesRepository phonesRepository, LogRepository logRepository) {
        this.ordersRepository = ordersRepository;
        this.phonesRepository = phonesRepository;
        this.logRepository = logRepository;
    }

    @Bean
    public CreateOrderUseCase createOrderUseCase() {
        return new CreateOrderUseCase(phonesRepository, ordersRepository, logRepository);
    }

}
