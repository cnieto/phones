package es.cnieto.phones.domain;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Phone {
    private final String id;
    private final BigDecimal price;
}
