package es.cnieto.phones.domain;

import java.util.List;

public interface OrdersRepository {
    Order persist(String name, String surname, String email, List<Phone> phones);
}
