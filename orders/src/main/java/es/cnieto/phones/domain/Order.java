package es.cnieto.phones.domain;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class Order {
    private final String name;
    private final String surname;
    private final String email;
    private final List<Phone> phones;

    public BigDecimal getTotalPrice() {
        return phones.stream()
                .map(Phone::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
