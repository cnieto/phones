package es.cnieto.phones.domain;

public interface LogRepository {
    void info(String message, Object... args);
}
