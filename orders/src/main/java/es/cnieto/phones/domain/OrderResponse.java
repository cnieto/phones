package es.cnieto.phones.domain;

import lombok.Data;

import java.util.LinkedList;
import java.util.List;

@Data
public class OrderResponse {
    private List<String> errors = new LinkedList<>();

    public boolean hasErrors() {
        return !errors.isEmpty();
    }

    void addPhoneNotFoundError(String phoneId) {
        errors.add(String.format("Phone{%s} not found", phoneId));
    }
}
