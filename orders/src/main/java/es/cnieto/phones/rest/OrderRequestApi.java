package es.cnieto.phones.rest;

import lombok.Data;

import java.util.List;

@Data
public class OrderRequestApi {
    private String name;
    private String surname;
    private String email;
    private List<String> phoneIds;
}
