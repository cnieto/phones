package es.cnieto.phones.rest;

import es.cnieto.phones.domain.CreateOrderUseCase;
import es.cnieto.phones.domain.OrderResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/orders")
@RequiredArgsConstructor
public class OrdersController {
    private final OrderRequestConverter orderRequestConverter;
    private final CreateOrderUseCase createOrderUseCase;

    @RequestMapping(method = RequestMethod.POST,
            produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> create(@RequestBody OrderRequestApi orderRequestApi) {
        OrderResponse orderResponse = createOrderUseCase.create(orderRequestConverter.from(orderRequestApi));

        if (orderResponse.hasErrors()) {
            return ResponseEntity.badRequest().body(String.join("\n", orderResponse.getErrors()));
        }

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
