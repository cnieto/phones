package es.cnieto.phones.rest;

import es.cnieto.phones.domain.OrderRequest;

public class OrderRequestConverter {
    public OrderRequest from(OrderRequestApi orderRequestApi) {
        return new OrderRequest(orderRequestApi.getName(),
                orderRequestApi.getSurname(),
                orderRequestApi.getEmail(),
                orderRequestApi.getPhoneIds());
    }
}
