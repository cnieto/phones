package es.cnieto.phones.repository.log;

import es.cnieto.phones.domain.LogRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LogSpringConfig {
    @Bean
    public LogRepository logRepository() {
        return new Slf4jLogRepository();
    }
}
