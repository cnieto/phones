package es.cnieto.phones.repository.log;

import es.cnieto.phones.domain.LogRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Slf4jLogRepository implements LogRepository {
    @Override
    public void info(String message, Object... args) {
        log.info(message, args);
    }
}
