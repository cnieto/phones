package es.cnieto.phones.repository.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Configuration
public class RestClientSpringConfiguration {
    private final URI phonesRestEndpoint;

    public RestClientSpringConfiguration(@Value("${phones.endpoint}") URI phonesRestEndpoint) {
        this.phonesRestEndpoint = phonesRestEndpoint;
    }

    @Bean
    public RestPhonesRepository restPhonesRepository() {
        return new RestPhonesRepository(new RestTemplate(), phonesRestEndpoint, phoneConverter());
    }

    @Bean
    PhoneConverter phoneConverter() {
        return new PhoneConverter();
    }
}
