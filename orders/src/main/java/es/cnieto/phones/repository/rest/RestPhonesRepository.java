package es.cnieto.phones.repository.rest;

import es.cnieto.phones.domain.Phone;
import es.cnieto.phones.domain.PhonesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Optional;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RequiredArgsConstructor
public class RestPhonesRepository implements PhonesRepository {
    private final RestTemplate restTemplate;
    private final URI phonesServiceEndpoint;
    private final PhoneConverter phoneConverter;

    @Override
    public Optional<Phone> findBy(String phoneId) {
        try {
            PhoneRest phoneRest = restTemplate.getForObject(phonesServiceEndpoint.resolve("/phones/" + phoneId), PhoneRest.class);

            return Optional.of(phoneConverter.from(phoneRest));
        } catch (HttpClientErrorException httpClientErrorException) {
            if (NOT_FOUND.equals(httpClientErrorException.getStatusCode())) {
                return Optional.empty();
            } else {
                throw httpClientErrorException;
            }
        }
    }
}
