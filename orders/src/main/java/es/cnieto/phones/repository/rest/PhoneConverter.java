package es.cnieto.phones.repository.rest;

import es.cnieto.phones.domain.Phone;

public class PhoneConverter {
    public Phone from(PhoneRest phoneRest) {
        return new Phone(phoneRest.getId(), phoneRest.getPrice());
    }
}
