package es.cnieto.phones.repository.dummy;

import es.cnieto.phones.domain.OrdersRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DummySpringConfig {
    @Bean
    public OrdersRepository ordersRepository() {
        return new OrdersDummyRespository();
    }
}
