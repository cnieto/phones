package es.cnieto.phones.repository.rest;

import lombok.Data;

import java.math.BigDecimal;
import java.net.URI;

@Data
public class PhoneRest {
    private String id;
    private URI image;
    private String name;
    private String description;
    private BigDecimal price;
}
