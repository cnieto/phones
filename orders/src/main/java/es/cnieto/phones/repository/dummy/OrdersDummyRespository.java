package es.cnieto.phones.repository.dummy;

import es.cnieto.phones.domain.Order;
import es.cnieto.phones.domain.OrdersRepository;
import es.cnieto.phones.domain.Phone;

import java.util.List;

public class OrdersDummyRespository implements OrdersRepository {
    @Override
    public Order persist(String name, String surname, String email, List<Phone> phones) {
        // TODO persist
        return new Order(name, surname, email, phones);
    }
}
