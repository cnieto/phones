package es.cnieto.phones.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Optional;

import static es.cnieto.phones.domain.OrderRequestTestBuilder.anOrderRequest;
import static es.cnieto.phones.domain.OrderTestBuilder.anOrder;
import static es.cnieto.phones.domain.PhoneTestBuilder.aPhone;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CreateOrderUseCaseTest {
    private static final String NAME = "name";
    private static final String SURNAME = "surname";
    private static final String EMAIL = "email@mail.com";
    private static final String PHONE_ONE_ID = "phone one id";
    private static final BigDecimal PHONE_ONE_PRICE = BigDecimal.valueOf(22.33);
    private static final String PHONE_TWO_ID = "phone two id";
    private static final BigDecimal PHONE_TWO_PRICE = BigDecimal.valueOf(45.11);
    private static final BigDecimal TOTAL_ORDER_PRICE = PHONE_ONE_PRICE.add(PHONE_TWO_PRICE);
    private static final String NON_EXISTENT_PHONE_ID = "non existent id";
    private static final String PHONE_IDS = PHONE_ONE_ID + ", " + PHONE_TWO_ID;
    private static final String LOG_MESSAGE = "Order for [name:{}][surname:{}][email:{}][phones:{}] created with a total price of {}";

    private CreateOrderUseCase createOrderUseCase;

    @Mock
    private OrdersRepository ordersRepository;
    @Mock
    private PhonesRepository phonesRepository;
    @Mock
    private LogRepository logRepository;

    @Before
    public void setup() {
        createOrderUseCase = new DomainSpringConfig(ordersRepository, phonesRepository, logRepository).createOrderUseCase();
    }

    @Test
    public void createSuccessfully() {
        mockPhonesRepository();
        mockOrdersRepository();

        OrderResponse orderResponse = createOrderUseCase
                .create(anOrderRequest()
                        .withName(NAME)
                        .withSurname(SURNAME)
                        .withEmail(EMAIL)
                        .withPhoneIds(asList(PHONE_ONE_ID, PHONE_TWO_ID))
                        .build());

        verifyHasNoErrors(orderResponse);
        verifyOrderPersisted();
        verify(logRepository).info(LOG_MESSAGE,
                NAME,
                SURNAME,
                EMAIL,
                PHONE_IDS,
                TOTAL_ORDER_PRICE);
    }

    @Test
    public void noCreatedWhenPhoneIsNotFound() {
        when(phonesRepository.findBy(NON_EXISTENT_PHONE_ID))
                .thenReturn(Optional.empty());

        OrderResponse orderResponse = createOrderUseCase
                .create(anOrderRequest()
                        .withPhoneIds(singletonList(NON_EXISTENT_PHONE_ID))
                        .build());

        assertTrue(orderResponse.hasErrors());
        assertThat(orderResponse.getErrors(),
                equalTo(singletonList("Phone{" + NON_EXISTENT_PHONE_ID + "} not found")));
        verifyZeroInteractions(ordersRepository, logRepository);
    }

    private void mockPhonesRepository() {
        when(phonesRepository.findBy(PHONE_ONE_ID))
                .thenReturn(Optional.of(firstPhone()));
        when(phonesRepository.findBy(PHONE_TWO_ID))
                .thenReturn(Optional.of(secondPhone()));
    }

    private Phone firstPhone() {
        return aPhone()
                .withId(PHONE_ONE_ID)
                .withPrice(PHONE_ONE_PRICE)
                .build();
    }

    private Phone secondPhone() {
        return aPhone()
                .withId(PHONE_TWO_ID)
                .withPrice(PHONE_TWO_PRICE)
                .build();
    }

    private void mockOrdersRepository() {
        when(ordersRepository
                .persist(NAME,
                        SURNAME,
                        EMAIL, asList(firstPhone(), secondPhone())))
                .thenReturn(anOrder()
                        .withName(NAME)
                        .withSurname(SURNAME)
                        .withEmail(EMAIL)
                        .withPhones(asList(firstPhone(), secondPhone()))
                        .build());
    }

    private void verifyHasNoErrors(OrderResponse orderResponse) {
        assertFalse(orderResponse.hasErrors());
        assertTrue(orderResponse.getErrors().isEmpty());
    }

    private void verifyOrderPersisted() {
        verify(ordersRepository)
                .persist(NAME,
                        SURNAME,
                        EMAIL,
                        asList(firstPhone(), secondPhone()));
    }

}