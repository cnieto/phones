package es.cnieto.phones.domain;

import java.math.BigDecimal;

public final class PhoneTestBuilder {
    private String id;
    private BigDecimal price;

    private PhoneTestBuilder() {
    }

    public static PhoneTestBuilder aPhone() {
        return new PhoneTestBuilder();
    }

    public PhoneTestBuilder withId(String id) {
        this.id = id;
        return this;
    }

    public PhoneTestBuilder withPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public Phone build() {
        return new Phone(id, price);
    }
}
