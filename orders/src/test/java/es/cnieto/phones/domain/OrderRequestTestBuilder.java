package es.cnieto.phones.domain;

import java.util.Collections;
import java.util.List;

public final class OrderRequestTestBuilder {
    private String name = "aName";
    private String surname = "aSurname";
    private String email = "anEmail";
    private List<String> phoneIds = Collections.singletonList("phoneIds");

    private OrderRequestTestBuilder() {
    }

    public static OrderRequestTestBuilder anOrderRequest() {
        return new OrderRequestTestBuilder();
    }

    public OrderRequestTestBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public OrderRequestTestBuilder withSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public OrderRequestTestBuilder withEmail(String email) {
        this.email = email;
        return this;
    }

    public OrderRequestTestBuilder withPhoneIds(List<String> phoneIds) {
        this.phoneIds = phoneIds;
        return this;
    }

    public OrderRequest build() {
        return new OrderRequest(name, surname, email, phoneIds);
    }
}
