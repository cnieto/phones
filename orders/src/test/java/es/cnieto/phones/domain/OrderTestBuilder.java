package es.cnieto.phones.domain;

import java.util.List;

public final class OrderTestBuilder {
    private String name;
    private String surname;
    private String email;
    private List<Phone> phones;

    private OrderTestBuilder() {
    }

    public static OrderTestBuilder anOrder() {
        return new OrderTestBuilder();
    }

    public OrderTestBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public OrderTestBuilder withSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public OrderTestBuilder withEmail(String email) {
        this.email = email;
        return this;
    }

    public OrderTestBuilder withPhones(List<Phone> phones) {
        this.phones = phones;
        return this;
    }

    public Order build() {
        return new Order(name, surname, email, phones);
    }
}
