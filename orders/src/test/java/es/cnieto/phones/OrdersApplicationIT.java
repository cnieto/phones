package es.cnieto.phones;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static es.cnieto.phones.rest.OrderRequestApiTestBuilder.anOrderRequestApi;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {
        "phones.endpoint=http://localhost:9850/",
})
public class OrdersApplicationIT {
    private static final String PHONES_PATH = "/phones/";
    private static final String PHONE_ID_ONE = "phoneIdOne";
    private static final String PHONE_ID_ONE_REST_RESPONSE = "{\"id\":\"1\",\"image\":\"http://image.one/\",\"name\":\"phone one\",\"description\":\"this is the phone one\",\"price\":233.2}";
    private static final String PHONE_ID_TWO = "phoneIdTwo";
    private static final String PHONE_ID_TWO_REST_RESPONSE = "{\"id\":\"1\",\"image\":\"http://image.one/\",\"name\":\"phone one\",\"description\":\"this is the phone one\",\"price\":233.2}";
    private static final String NON_EXISTENT_ID = "nonExistentId";
    @Rule
    public WireMockRule wireMockRule = new WireMockRule(wireMockConfig().port(9850));
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void contextLoads() {
    }

    @Test
    public void createOrder() {
        stubPhonesServicesFor(PHONE_ID_ONE, PHONE_ID_ONE_REST_RESPONSE);
        stubPhonesServicesFor(PHONE_ID_TWO, PHONE_ID_TWO_REST_RESPONSE);

        ResponseEntity<Void> responseEntity = restTemplate
                .postForEntity("/orders",
                        anOrderRequestApi()
                                .withPhoneIds(asList(PHONE_ID_ONE, PHONE_ID_TWO))
                                .build(),
                        Void.class);

        assertThat(responseEntity.getStatusCode(), equalTo(HttpStatus.CREATED));
    }


    @Test
    public void createOrderWithNonExistentPhone() {
        stubPhonesServicesReturning404For(NON_EXISTENT_ID);

        ResponseEntity<String> responseEntity = restTemplate
                .postForEntity("/orders", anOrderRequestApi()
                                .withPhoneIds(singletonList(NON_EXISTENT_ID))
                                .build(),
                        String.class);

        assertThat(responseEntity.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
        assertThat(responseEntity.getBody(), equalTo("Phone{" + NON_EXISTENT_ID + "} not found"));
    }

    private void stubPhonesServicesFor(String phoneId, String restResponse) {
        wireMockRule.stubFor(
                get(urlEqualTo(PHONES_PATH + phoneId))
                        .willReturn(aResponse()
                                .withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                                .withBody(restResponse)));
    }

    private void stubPhonesServicesReturning404For(String phoneId) {
        wireMockRule.stubFor(
                get(urlEqualTo(PHONES_PATH + phoneId))
                        .willReturn(aResponse()
                                .withStatus(NOT_FOUND.value())
                                .withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)));
    }

}