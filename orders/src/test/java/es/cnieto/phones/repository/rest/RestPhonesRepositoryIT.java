package es.cnieto.phones.repository.rest;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import es.cnieto.phones.domain.Phone;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Optional;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static es.cnieto.phones.domain.PhoneTestBuilder.aPhone;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@TestPropertySource(properties = {
        "phones.endpoint=http://localhost:9899/",
})
public class RestPhonesRepositoryIT {
    private static final String PHONES_PATH = "/phones/";
    private static final String PHONE_ID = "1";
    private static final BigDecimal PHONE_PRICE = BigDecimal.valueOf(233.2);
    private static final String REST_PHONE_RESPONSE = "{\"id\":\"1\",\"image\":\"http://image.one/\",\"name\":\"phone one\",\"description\":\"this is the phone one\",\"price\":233.2}";
    @Rule
    public WireMockRule wireMockRule = new WireMockRule(wireMockConfig().port(9899));

    @Autowired
    private RestPhonesRepository restPhonesRepository;

    @Test
    public void callSuccessfully() {
        wireMockRule.stubFor(
                get(urlEqualTo(PHONES_PATH + PHONE_ID))
                        .willReturn(aResponse()
                                .withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                                .withBody(REST_PHONE_RESPONSE)));

        Optional<Phone> phoneOptional = restPhonesRepository.findBy(PHONE_ID);

        assertThat(phoneOptional,
                equalTo(Optional.of(
                        aPhone().withId(PHONE_ID)
                                .withPrice(PHONE_PRICE)
                                .build())));
    }

    @Test
    public void callWithNonExistentPhone() {
        wireMockRule.stubFor(
                get(urlEqualTo(PHONES_PATH + PHONE_ID))
                        .willReturn(aResponse()
                                .withStatus(NOT_FOUND.value())
                                .withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)));

        Optional<Phone> phoneOptional = restPhonesRepository.findBy(PHONE_ID);

        assertFalse(phoneOptional.isPresent());
    }
}
