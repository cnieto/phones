package es.cnieto.phones.rest;

import java.util.List;

import static java.util.Arrays.asList;

public final class OrderRequestApiTestBuilder {
    private String name = "name";
    private String surname = "surname";
    private String email = "e@mail.com";
    private List<String> phoneIds = asList("phoneIdOne", "phoneIdTwo");


    private OrderRequestApiTestBuilder() {
    }

    public static OrderRequestApiTestBuilder anOrderRequestApi() {
        return new OrderRequestApiTestBuilder();
    }

    public OrderRequestApiTestBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public OrderRequestApiTestBuilder withSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public OrderRequestApiTestBuilder withEmail(String email) {
        this.email = email;
        return this;
    }

    public OrderRequestApiTestBuilder withPhoneIds(List<String> phoneIds) {
        this.phoneIds = phoneIds;
        return this;
    }

    public OrderRequestApi build() {
        OrderRequestApi orderRequestApi = new OrderRequestApi();
        orderRequestApi.setName(name);
        orderRequestApi.setSurname(surname);
        orderRequestApi.setEmail(email);
        orderRequestApi.setPhoneIds(phoneIds);
        return orderRequestApi;
    }
}
