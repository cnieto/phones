package es.cnieto.phones.rest;

import es.cnieto.phones.domain.OrderRequest;
import org.junit.Test;

import static es.cnieto.phones.domain.OrderRequestTestBuilder.anOrderRequest;
import static es.cnieto.phones.rest.OrderRequestApiTestBuilder.anOrderRequestApi;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class OrderRequestConverterTest {
    private static final String NAME = "name";
    private static final String SURNAME = "surname";
    private static final String EMAIL = "email@none.com";
    private static final String PHONE_ID = "phone id";

    private OrderRequestConverter orderRequestConverter = new RestSpringConfig().orderRequestConverter();

    @Test
    public void convertSuccessfully() {
        OrderRequestApi orderRequestApi = anOrderRequestApi()
                .withName(NAME)
                .withSurname(SURNAME)
                .withEmail(EMAIL)
                .withPhoneIds(singletonList(PHONE_ID))
                .build();

        OrderRequest orderRequest = orderRequestConverter.from(orderRequestApi);

        assertThat(orderRequest, equalTo(
                anOrderRequest()
                        .withName(NAME)
                        .withSurname(SURNAME)
                        .withEmail(EMAIL)
                        .withPhoneIds(singletonList(PHONE_ID))
                        .build()));
    }

}