# Phone App
````
1 Create a endpoint to retrieve the phone catalog, and pricing.
* it returns a collection of phones, and their prices.
* each phone should contain a reference to its image, the name, the description, and
its price.

2 OPTIONAL - Create and endpoints to check and create and order.
* receives and order that contains the customer information name, surname, and
email, and the list of phones that the customer wants to buy.
* Calculate the total prices of the order.
* log the final order to the console.

- it should have tests.
- How would you improve the system?
- How would you avoid your order api to be overflow?

Bonus Points for Dockerizing the app
Extra Bonus Points: the second endpoint use the first endpoint to validate the order.
Extra Bonus Points for using a microservice approach.
````
## Approach
To do this, I decide to create two microservices with _Spring Boot_, _Java 8_, and using _Maven_ for build the docker images.
Both are implemented using  hexagonal architecture in order to avoid mixing between business and technological decisions.
## Build & Run
### Create docker images
To build the docker images, on each directory execute `mvnw clean install`

### Start docker images
To run the docker images linking _Orders Microservice_ with _Phones Microservice_, on the root path execute `docker-compose up`.

With this, both microservices will be deployed on docker exposing the port _8081_ for _Phones Microservice_ and _8080_ for _Orders Microservice_.

### Example request
```
curl -X GET \
  http://localhost:8081/phones/1
```
```
curl -X GET \
  http://localhost:8081/phones/1
```
```
curl -X POST \
  http://localhost:8080/orders \
  -H 'Content-Type: application/json' \
  -d '{
"name": "John",
"surname": "Smith",
"email": "none@gmail.com",
"phoneIds": ["1", "2"]
}'
```

### Observations
I use [Lombok Library](https://projectlombok.org/) to build the project, to compile the project using the IDE it's required to install the plugin.

## How would you improve the system?
- The first improvement, it's to use a real persistence layer for _Phone Microservice_, because actually it's using fixed values on memory. When this is done, the _PhonesApplicationIT_ test should be changed to simulate the new persistence layer instead of the repository.
- Add a real layer of persistence for the Order and a GET method, with this the order creation endpoint could return the location of the new resource.
- Add a data validation on _Orders Microservice_, like avoid null values or email validation.
- Add _Swagger_ to document microservices could be a good improvement too.

## How would you avoid your order api to be overflow?
To avoid overflow, maybe it's a solution to use a kind of messaging approach, someone who need to create an order publish a message, and the order microservice will process it on an asynchronous way. 
